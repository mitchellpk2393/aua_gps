# from machine import UART
import utime
from pyb import UART

# C:\Users\MitchellK\PycharmProjects\uGPS\venv\Scripts\python.exe C:\Users\MitchellK\AppData\Roaming\JetBrains\PyCharmCE2020.1\plugins\intellij-micropython/scripts/microupload.py -C C:/Users/MitchellK/PycharmProjects/uGPS -v COM8 C:/Users/MitchellK/PycharmProjects/uGPS/main.py

uart = UART(4)
print(uart)
uart.init(9600, rxbuf=64)
print(uart)


# uart.init(9600, bits=8, parity=None, stop=1)

def wait_for_response(expected_response):
    prev = uart.any()
    for counter in range(50):
        utime.sleep_ms(20)
        curr = uart.any()

        if curr != prev:
            prev = curr
            continue
        else:
            response = uart.readline()
            if response is None:
                continue

            response = response.decode('utf8').strip()
            print(response)
            if expected_response in response:
                print(response)
                return 1
            pass
        pass
    return 0

def send_command(payload):
    print(payload)
    payload = bytearray(payload, 'ascii')
    uart.write(payload)
    return


for _ in range(1000):
    send_command('ATE0\r\n')
    wait_for_response('OK')

    send_command('AT+CPIN?\r\n')
    wait_for_response('+CPIN: READY')

    send_command('AT+CSQ\r\n')
    wait_for_response('+CSQ: ')

    send_command('AT+CREG?\r\n')
    rx = wait_for_response('+CREG: 0,1')

    if rx == 0: continue

    for _ in range(1000):
        utime.sleep_ms(100)

        send_command('AT+CGATT?\r\n')
        wait_for_response('+CGATT: 1')

        send_command('AT+CIPSHUT\r\n')
        wait_for_response('SHUT OK')

        send_command('AT+CSTT="CMNET"\r\n')
        wait_for_response('OK')

        send_command('AT+CIICR\n')
        wait_for_response('OK')

        send_command('AT+CIPSTART="TCP","77.248.232.160","5050"\r\n')
        wait_for_response('CONNECT OK')

        msg = 'Hello World'
        send_command('AT+CIPSEND={:d}\r\n'.format(len(msg)))
        wait_for_response('>')

        send_command(msg)
        wait_for_response('')
    pass
