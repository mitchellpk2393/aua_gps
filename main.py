import pyb
import utime
import uhashlib
import uasyncio as asyncio
import aes

# from aua_gps import aes # https://github.com/boppreh/aes
# from machine import UART
from pyb import UART

print("####BOOT UP######")

led = pyb.LED(2)
led.on()
header_NMEA = "$GPRMC"

tcp_head = "HEAD#".encode()
tcp_tail = "#TAIL".encode()
PORT = 5050  # above 4000 maybe no in use
CLIENT_ID = "1234#"

PASS_CODE = "DERWIN".encode()
KEY = uhashlib.sha256(PASS_CODE).digest()
iv = 'this is an IV456'.encode()

# TODO cipher data mester ta den fcking bytes !!

###############################################################
uart_gps = UART(6, 9600)
uart_gps.init(9600, bits=8, parity=None, stop=1)
uart_sim = UART(4)
uart_sim.init(9600, rxbuf=64)

#### GLOBALS VAR
rx_data = bytes([])
gps_buffer = []
sim_pin = 0
net_set = 0
gprs_set = 0
ip_on = 0


def weekday(y, m, d):
    try:
        wday = 0
        y = int(y)
        m = int(m)
        d = int(d)
        wday = (d + ((153 * (m + 12 * ((14 - m) / 12) - 3) + 2) / 5)
                + (365 * (y + 4800 - ((14 - m) / 12)))
                + ((y + 4800 - ((14 - m) / 12)) / 4)
                - ((y + 4800 - ((14 - m) / 12)) / 100)
                + ((y + 4800 - ((14 - m) / 12)) / 400)
                - 32045) % 7
        return round(wday)
    except TypeError:
        print("error int to string object")
        print("var are not integer")


def set_RTC_now(segments):
    import pyb
    rtc = pyb.RTC()
    # print(segments)

    # UTC offset +2
    hour = int(segments[1]) + 2
    min = int(segments[2])
    second = int(segments[3])

    y = int(str(20) + (segments[11]))
    m = int(segments[10])
    d = int(segments[9])
    # go find the weekday of the given date
    w_day = weekday(y, m, d)

    tup_temp = (y, m, d, w_day, hour, min, second, 0)
    try:
        rtc.datetime(tup_temp)
        print("LIVE TIME IS SETUP")
        # print("rtc lib", rtc.datetime())
        print("utime lib", utime.localtime())
    except TypeError as e:
        print("ERROR SETTING RTC->", e)


def get_gps_info():
    global day, month, year, ss, mm, hh, my_list, rx, GNSS_ava, NMEA_buff, GNSS_info
    my_list = ()
    if uart_gps.any():
        rx_data = uart_gps.read()
        # print(type(rx_data),rx_data)
        try:
            rx_data = rx_data.decode('utf8')
        except UnicodeError:
            tmp = bytearray(rx_data)
            for i in range(len(tmp)):
                if tmp[i] > 127:
                    tmp[i] = ord('#')
            rx_data += bytes(tmp).decode('utf8')
        received_data = rx_data
        # received_data = str(rx_data)
        # print(type(received_data),received_data)
        # TODO indexerror when gps turn off and on
        try:
            GNSS_ava = received_data.find(header_NMEA)
            # typeerror accured sometimes here cant covnert str to byte
            # print(GNSS_ava)
            if GNSS_ava >= 0:
                # GNSS_info = received_data.split(header_NMEA, 1)[1]
                GNSS_info = received_data.split('\r', 1)[0]
                # print(type(GNSS_info),GNSS_info)
                NMEA_buff = GNSS_info.split(',')
                # print(type(NMEA_buff),NMEA_buff)
                # print(NMEA_buff)
                gps_date = NMEA_buff[9]
                UTC = NMEA_buff[1]
                is_valid = NMEA_buff[2]
                raw_lat = NMEA_buff[3]
                N_S = NMEA_buff[4]
                raw_long = NMEA_buff[5]
                E_W = NMEA_buff[6]
                gps_frame = NMEA_buff[0]
                hh = (UTC[:2])
                mm = (UTC[2:4])
                ss = (UTC[4:6])
                day = (gps_date[:2])
                month = (gps_date[2:4])
                year = (gps_date[4:6])
                my_list = (gps_frame, hh, mm, ss, is_valid, raw_lat, N_S, raw_long, E_W, day, month, year)
        except TypeError:
            pass
        # try:
        #     gps_date = NMEA_buff[9]
        #     UTC = NMEA_buff[1]
        #     is_valid = NMEA_buff[2]
        #     raw_lat = NMEA_buff[3]
        #     N_S = NMEA_buff[4]
        #     raw_long = NMEA_buff[5]
        #     E_W = NMEA_buff[6]
        #     gps_frame = NMEA_buff[0]
        #     hh = (UTC[:2])
        #     mm = (UTC[2:4])
        #     ss = (UTC[4:6])
        #     day = (gps_date[:2])
        #     month = (gps_date[2:4])
        #     year = (gps_date[4:6])
        #     my_list = (gps_frame, hh, mm, ss, is_valid, raw_lat, N_S, raw_long, E_W, day, month, year)
        # except IndexError:
        #     pass

        # print(len(my_list))
        # print(my_list[4])
        # print('list =', my_list,(type(my_list)))
        if len(my_list) == 12 and my_list[4] == 'A':
            return my_list


def accel_init():
    import pyb
    accel = pyb.Accel()
    print("X", accel.x(), "Y", accel.y(), "Z", accel.z())


async def commands(command, expected_response=None, timeout=50):
    # Send command as ascii
    command = bytearray(command, 'ascii')
    uart_sim.write(command)
    print("UARTSEND:", command)

    prev = uart_sim.any()
    for counter in range(timeout):
        # utime.sleep_ms(20)
        await asyncio.sleep_ms(20)
        curr = uart_sim.any()

        if curr != prev:
            prev = curr
            continue
        else:
            response = uart_sim.readline()
            if response is None:
                continue
            try:
                response = response.decode('utf8').strip()
                print('raw', response)
            except UnicodeError:
                tmp = bytearray(response)
                for i in range(len(tmp)):
                    if tmp[i] > 127:
                        tmp[i] = ord('#')
                response += bytes(tmp).decode('utf8').strip()
            if expected_response in response:
                print('valid', response)
                return 1
            pass
        pass
    return 0


sim_error = 0
try_tcp = 0


async def crypt_send(vec, buff):
    # TODO try to send everything in one payload, vec should be append first without encrypt then follow gps encrypted
    global sim_error, try_tcp
    # join list togheter the form gps sentence
    try_tcp += 1
    print("current tcp error ={0:d}//try are{1:d}".format(sim_error, try_tcp))

    gps = ','.join(buff)
    gps = CLIENT_ID.encode() + gps.encode()
    # print(gps, type(gps))
    gps = aes.AES(KEY).encrypt_cbc(gps, vec)
    print()
    payload = tcp_head + iv + gps + tcp_tail
    # print("the payload={}".format(payload))
    length = len(payload)
    # print("msg len={}".format(length))
    TCP: bool = True
    while TCP:
        if not await commands('AT+CIPSTART="TCP","77.248.232.160","5050"\r\n', 'CONNECT'):
            sim_error += 1
            continue

        at = ("AT+CIPSEND={0}\r\n".format(len(str(length))))
        if await commands(at, ">"):
            # print('TX msg len', (len(str(length))))
            # problem with send int type over TCP
            if not await commands(bytes(str(length), 'utf-8'), "SEND OK", 220):
                sim_error += 1
                continue
            at = ("AT+CIPSEND={0}\r\n".format(length))

            if await commands(at, '>'):
                start = utime.ticks_ms()
                if not await commands(payload, 'SEND OK', 220):
                    sim_error += 1
                    continue
                print("sending took{0}".format(utime.ticks_ms() - start))
                await commands('AT+CIPCLOSE\r\n', 'CLOSE OK')
                TCP = False
                return 1
                # TODO would it be nice to have a specific timeout for error sending tcp data
                # await commands('0x1a','SEND OK') not working should be 1A only didnt test it yet


def echo_off():
    return commands('ATE0\r\n', "OK")


def sim_reset():
    return commands("AT+CFUN=1,1\r\n", "OK")


async def established_TCP(timeout):
    global sim_pin, net_set, ip_on
    while not sim_pin and not net_set and not ip_on:
        ###########################################
        if not await commands('ATE0\r\n', 'OK'): continue

        while not sim_pin:
            sim_pin = await commands('AT+CPIN?\r\n', '+CPIN: READY')
            print('simpin=', sim_pin)

        while not net_set:
            await commands('AT+CSQ\r\n', '+CSQ: ')
            if not await commands('AT+CREG?\r\n', '+CREG: 0,1'): continue

            await commands('AT+CIPSHUT\r\n', 'SHUT OK')

            await commands('AT+CIPMUX=0\r\n', 'OK')

            net_set = await commands('AT+CSTT="CMNET"\r\n', 'OK')
            print("net=", net_set)
        #
        while not ip_on:
            await commands('AT+CIICR\r\n', 'OK')
            ip_on = await commands('AT+CIFSR\r\n', '.')

    return 1
    # send_command("AT+CIPSTATUS\r\n")
    # response = await wait_for_response("STATE: IP GPRSACT")
    # print("respone", response)
    # if response == 0: continue


#
# # @function to receive gps data threw UART6
async def get_NMEA():
    global gps_buffer
    while 1:
        gps = get_gps_info()
        if not gps:
            continue  # Skip

        gps_buffer += [{
            'gps': gps,
            'read': {},
        }]
        print(len(gps_buffer))
        print()

        await asyncio.sleep_ms(100)
        pass


# @func to send GPS data to the server by using SIM module
async def server_conn():
    global gps_buffer, sim_error
    now = utime.ticks_ms()

    etime = []
    while 1:

        if len(gps_buffer) == 0: continue  # Skip
        # print("buf", gps_buffer)
        data = gps_buffer[::-1][-1]  # Reverse and select first element
        # print("data", data)
        gps = data['gps']
        data['read'][0] = 1
        print("avg run t={0:8.2f}".format(((utime.ticks_ms() - now) / 1000)))
        check = await crypt_send(iv, gps)
        # print('check', check)
        if not check:
            pass

        # data = gps_buffer[::-1][-1]  # Reverse and select first element
        # # print("data", data)
        # gps = data['gps']
        # data['read'][0] = 1
        # TODO but delay here
        await asyncio.sleep_ms(900)
        await asyncio.sleep_ms(100)
        pass


# @func to update RTC for the MCU,  parameters are y,m,d,hh,mm,ss
async def update_rtc():
    global gps_buffer
    while 1:
        if len(gps_buffer) == 0: continue  # Skip
        data = gps_buffer[::-1][-1]  # Reverse and select first element
        gps = data['gps']
        # return a value from rtc_now then mark read code??????????
        set_RTC_now(gps)
        data['read'][1] = 2

        # print(gps)
        await asyncio.sleep_ms(100)
        pass


async def cache_clearing():
    global gps_buffer, item
    while 1:
        if len(gps_buffer) == 0: continue  # Skip
        if len(gps_buffer) > 7: gps_buffer.pop(-len(gps_buffer))  # avoid memory error
        try:
            for i in range(len(gps_buffer)):
                # Get element
                # print(gps_buffer[i])
                item = gps_buffer[i]
                if len(item['read']) == 2:
                    del gps_buffer[i]
                    pass
                pass
        except IndexError:
            pass
            # Remove already read data
        await asyncio.sleep_ms(100)
        pass


async def main():
    check = await established_TCP(500)
    if check:
        loop = asyncio.get_event_loop()
        loop.create_task(get_NMEA())
        loop.create_task(server_conn())
        loop.create_task(update_rtc())
        loop.create_task(cache_clearing())
        loop.run_forever()

    # check = await established_TCP(500)
    # if check:
    # asyncio.create_task(get_NMEA())
    # asyncio.create_task(server_conn())
    # asyncio.create_task(update_rtc())
    # asyncio.create_task(cache_clearing())
    # await asyncio.sleep_ms(10000_000)


asyncio.run(main())
# check = await established_TCP(500)
# if check:
#     loop = asyncio.get_event_loop()
#     loop.create_task(get_NMEA())
#     loop.create_task(server_conn())
#     loop.create_task(update_rtc())
#     loop.create_task(cache_clearing())
#     loop.run_forever()
# loop.run_until_complete(asyncio.gather(get_NMEA(), server_conn(), update_rtc(), cache_clearing()))

# await sim_reset()

# sim_pin = 0
# pin_nr = 0
# net_set = 0
# gprs_set = 0
# response = 0
# ip_on = 0
# while 1:
#     for _ in range(50):
#         ###########################################
#
#         if not await commands('ATE0\r\n', 'OK'): break
#
#         while not sim_pin:
#             sim_pin = await commands('AT+CPIN?\r\n', '+CPIN: READY')
#             print('simpin=', sim_pin)
#
#         while not net_set:
#             await commands('AT+CSQ\r\n', '+CSQ: ')
#             if not await commands('AT+CREG?\r\n', '+CREG: 0,1'): continue
#
#             await commands('AT+CIPSHUT\r\n', 'SHUT OK')
#
#             await commands('AT+CIPMUX=0\r\n', 'OK')
#
#             net_set = await commands('AT+CSTT="CMNET"\r\n', 'OK')
#             print("net=", net_set)
#         #
#
#         while not ip_on:
#             await commands('AT+CIICR\r\n', 'OK')
#
#             ip_on = await commands('AT+CIFSR\r\n', '.')
#
#         # send_command("AT+CIPSTATUS\r\n")
#         # response = await wait_for_response("STATE: IP GPRSACT")
#         # print("respone", response)
#         # if response == 0: continue
#
#         for counter in range(5):
#             utime.sleep_ms(5000)
#
#             if not await commands('AT+CIPSTART="TCP","77.248.232.160","5050"\r\n', 'CONNECT'): continue
#
#             # msg = 'HEAD#END#TAIL'
#             # if not await commands('AT+CIPSEND={:d}\r\n'.format(len(msg)), '>'): continue
#
#             # await commands(msg, 'SEND OK')
#             await crypt_send(iv, "$GPRMC,181613.000,A,5153.8593,N,00429.7746,E,0.2,145.7,080720,,,A*62")
#
#             # await commands('0x1a\r\n', 'SEND OK')
#             await commands('AT+CIPCLOSE\r\n', 'CLOSE OK')
#
#     pass

# return


# Running on a generic board
# sim_reset()
# utime.sleep_ms(4000)
# asyncio.run(main())
