import pyb
import utime
import uhashlib
import uasyncio as asyncio

# from aua_gps import aes # https://github.com/boppreh/aes
# from machine import UART
from pyb import UART

print("####BOOT UP######")

led = pyb.LED(2)
led.on()
header_NMEA = "$GPRMC"

tcp_head = "HEAD#".encode()
tcp_tail = "#TAIL".encode()
PORT = 5050  # above 4000 maybe no in use
CLIENT_ID = "1234#"

PASS_CODE = "DERWIN".encode()
KEY = uhashlib.sha256(PASS_CODE).digest()
iv = 'this is an IV456'.encode()

# TODO cipher data mester ta den fcking bytes !!

###############################################################
# uart = UART(6, 9600)
# uart.init(9600, bits=8, parity=None, stop=1)
uart_sim = UART(4)
uart_sim.init(9600, rxbuf=64)

#### GLOBALS VAR
tx_data = bytearray(100)
gps_buffer = []


def weekday(y, m, d):
    try:
        wday = 0
        y = int(y)
        m = int(m)
        d = int(d)
        wday = (d + ((153 * (m + 12 * ((14 - m) / 12) - 3) + 2) / 5)
                + (365 * (y + 4800 - ((14 - m) / 12)))
                + ((y + 4800 - ((14 - m) / 12)) / 4)
                - ((y + 4800 - ((14 - m) / 12)) / 100)
                + ((y + 4800 - ((14 - m) / 12)) / 400)
                - 32045) % 7
        return round(wday)
    except TypeError:
        print("error int to string object")
        print("var are not integer")


def set_RTC_now(segments):
    import pyb
    rtc = pyb.RTC()
    print(segments)
    # UTC offset +2
    hour = segments[0] + 2
    min = segments[1]
    second = segments[2]

    y = int(str(20) + str(segments[10]))
    m = segments[9]
    d = segments[8]
    # go find the weekday of the given date
    w_day = weekday(y, m, d)

    tup_temp = (y, m, d, w_day, hour, min, second, 0)
    try:
        rtc.datetime(tup_temp)
        print("LIVE TIME IS SETUP")
        print("rtc lib", rtc.datetime())
        print("utime lib", utime.localtime())
    except TypeError as e:
        print("ERROR", e)


def get_gps_info():
    if uart.any():
        uart.readinto(tx_data, 80)
        received_data = str(tx_data)
        GNSS_ava = received_data.find(header_NMEA)

        if GNSS_ava > 0:
            GNSS_info = received_data.split(header_NMEA, 1)[1]
            GNSS_info = GNSS_info.split('\\r', 1)[0]
            NMEA_buff = GNSS_info.split(',')
            # get date and time from sentence
            gps_date = NMEA_buff[9]
            UTC = NMEA_buff[1]
            is_valid = NMEA_buff[2]
            raw_lat = NMEA_buff[3]
            N_S = NMEA_buff[4]
            raw_long = NMEA_buff[5]
            E_W = NMEA_buff[6]
            hh = int((UTC[:2]))
            mm = int((UTC[2:4]))
            ss = int((UTC[4:6]))
            day = int((gps_date[:2]))
            month = int((gps_date[2:4]))
            year = int(gps_date[4:6])
            my_list = (hh, mm, ss, is_valid, raw_lat, N_S, raw_long, E_W, day, month, year)
            # print(my_list)
            return my_list


def accel_init():
    import pyb
    accel = pyb.Accel()
    print("X", accel.x(), "Y", accel.y(), "Z", accel.z())


def commands(command, expected_response=None):
    # Send command as ascii
    command = bytearray(command, 'ascii')
    # print(type(command))
    uart_sim.write(command)

    # if expected_response is None:
    #     return 0
    #
    # result = 0

    # respone = ''

    # for counter in range(100):
    #     # Wait for response (continue running other async functions)
    #     # await asyncio.sleep_ms(10)
    #     utime.sleep_ms(11)
    #
    #     # Get response from sim module
    #     response = uart_sim.readline()
    #
    #     if response is None:
    #         continue
    #
    #     # Decode response
    #     # try:
    #     response = response.decode('utf8').strip()
    #     print("raw", response)
    #     # except UnicodeError:
    #     #     tmp = bytearray(response)
    #     #     for i in range(len(tmp)):
    #     #         if tmp[i] > 127:
    #     #             tmp[i] = ord('#')
    #     #     respone += bytes(tmp).decode('utf8').strip()
    #
    #     if expected_response in response:
    #         print('valid', response)
    #         result = 1
    #         break
    #     pass
    # return result


def wait_for_tcp(expected_response):
    # prev = uart.any()
    result = 0
    for counter in range(100):
        utime.sleep_ms(11)
        # curr = uart_sim.any()

        # if curr != prev:
        #     prev = curr
        #     continue
        # else:
        response = uart_sim.readline()
        if response is None:
            continue
        try:
            response = response.decode('utf8').strip()
            print('raw',response)
        except UnicodeError:
            tmp = bytearray(response)
            for i in range(len(tmp)):
                if tmp[i] > 127:
                    tmp[i] = ord('#')
            response += bytes(tmp).decode('utf8').strip()
        if expected_response in response:
            print('valid',response)
            result = 1
            break
        pass
    return result


def crypt_send(vec, gps):
    # TODO try to send everything in one payload, vec should be append first without encrypt then follow gps encrypted
    gps = CLIENT_ID.encode() + gps
    gps = aes.AES(KEY).encrypt_cbc(gps, vec)
    print("encrypted={}".format(gps))
    print()
    payload = tcp_head + iv + gps + tcp_tail
    print("the payload={}".format(payload))
    length = len(payload)
    print("the lenght={}".format(length))
    breakpoint()
    at = ("AT+CIPSEND={:d}\r\n".format(length))
    if commands(at, ">"):
        return commands(gps, "SEND OK")
        # would it be nice to have a specific timeout for specific command like when sending msg to server
    else:
        return 0


def echo_off():
    return commands('ATE0\r\n', "OK")


async def sim_set_apn(apn):
    at = ('AT+CSTT={}\r\n'.format(apn))
    if commands(at, "OK"):
        # print("apn")
        ip_status = commands("AT+CIICR\n", "OK")
        return ip_status
        # commands("AT+CIFSR\n",".")
    else:
        return 0


def sim_reset():
    return commands("AT+CFUN=1,1\r\n", "OK")


#
# # @function to receive gps data threw UART6
async def get_NMEA():
    global gps_buffer
    while 1:
        gps = get_gps_info()
        if not gps:
            continue  # Skip

        gps_buffer += [{
            'gps': gps,
            'read': {},
        }]
        # print(gps_buffer)
        await asyncio.sleep_ms(100)
        pass


# @func to send GPS data to the server by setting up UART
async def server_conn():
    global gps_buffer
    while 1:
        if len(gps_buffer) == 0: continue  # Skip
        # print("buf", gps_buffer)
        data = gps_buffer[::-1][-1]  # Reverse and select first element
        # print("data", data)
        gps = data['gps']
        data['read'][0] = 1

        await asyncio.sleep_ms(100)
        pass


# @func to update RTC for the MCU,  parameters are y,m,d,hh,mm,ss
async def update_rtc():
    global gps_buffer
    while 1:
        if len(gps_buffer) == 0: continue  # Skip
        data = gps_buffer[::-1][-1]  # Reverse and select first element
        gps = data['gps']
        # return a value from rtc_now then mark read code??????????
        set_RTC_now(gps)
        data['read'][1] = 2

        # print(gps)
        await asyncio.sleep_ms(100)
        pass


async def cache_clearing():
    global gps_buffer
    while 1:
        if len(gps_buffer) == 0: continue  # Skip
        for i in range(len(gps_buffer)):
            # Get element
            item = gps_buffer[i]

            # Remove already read data
            if len(item['read']) == 2:
                del gps_buffer[i]
                pass
            pass
        await asyncio.sleep_ms(100)
        pass


def str_2_byt(char):
    return bytearray(char, 'ascii')


async def main():
    #     asyncio.create_task(get_NMEA())
    #     asyncio.create_task(server_conn())
    #     asyncio.create_task(update_rtc())
    #     asyncio.create_task(cache_clearing())
    # await sim_reset()
    # utime.sleep_ms(5000)
    echo_off()
    sim_pin = 0
    pin_nr = 0
    nr_pin = 0
    net_set = 0
    gprs_set = 0
    ip_shut = 0
    apn_set = 0
    ip_on = 0
    while 1:
        for _ in range(50):
            # if not commands(str_2_byt('AT+CPIN?\r\n'), '+CPIN: READY'): break  # Init SIM
            # if not commands(str_2_byt('AT+CREG?\r\n'), '+CREG: 0,1'): break
            # if not commands(str_2_byt('AT+CGATT?\r\n'), '+CGATT: 1'): break
            # if not commands(str_2_byt('AT+CIPSHUT\r\n'), 'SHUT OK'): break
            # if not commands(str_2_byt('AT+CSTT="CMNET"\r\n'), 'OK'): break  # APN]
            # if not commands(str_2_byt('AT+CIICR\n'), 'OK'): break
            # #check = wait_for_tcp(str_2_byt('AT+CIPSTART="TCP","77.248.232.160","5050"\r\n'), 'CONNECT OK')
            # #print("check=", check)
            # payload = bytearray('AT+CIPSTART="TCP","77.248.232.160","5050"\r\n', 'ascii')
            # uart_sim.write(payload)
            # wait_for_tcp('CONNECT OK')
            # #print("check=", check)
            # msg = 'HEAD#END#TAIL'
            # payload = bytearray('AT+CIPSEND={:d}\r\n'.format(len(msg)), 'ascii')
            # uart_sim.write(payload)
            # wait_for_tcp('>')
            #
            # payload = bytearray(msg, 'ascii')
            # uart_sim.write(payload)
            # wait_for_tcp('')
            # print("trying to send")
            # data = 'HEAD#END#TAIL'
            # if not commands(str_2_byt('AT+CIPSEND={:d}\r\n'.format(len(data))), '>'): pass
            # commands(str_2_byt(data), '')
            ###########################################
            #commands(('ATE0\r\n'), '+CPIN: READY')
            payload = bytearray('ATE0\r\n', 'ascii')
            uart_sim.write(payload)
            if not wait_for_tcp('OK'): break
            # while not pin_nr:
            #     payload = bytearray('AT+CPIN="0000"\r\n','ascii')
            #     uart_sim.write(payload)
            #     pin_nr =  wait_for_tcp('OK')
            # while not nr_pin:
            #     payload = bytearray('AT+CPIN="0000"\r\n','ascii')
            #     uart_sim.write(payload)
            #     nr_pin =  wait_for_tcp('OK')
            #commands('AT+CPIN?\r\n', '+CPIN: READY')
            while not sim_pin:
                payload = bytearray('AT+CPIN?\r\n', 'ascii')
                uart_sim.write(payload)
                sim_pin = wait_for_tcp('+CPIN: READY')
                print('simpin=',sim_pin)

            # #commands('AT+CREG?\r\n', '+CPIN: READY')
            while not net_set:
                payload = bytearray('AT+CREG?\r\n', 'ascii')
                uart_sim.write(payload)
                net_set = wait_for_tcp('+CREG: 0,1')
                utime.sleep_ms(200)
                print("net=",net_set)
            #
            # #commands('AT+CGATT?\r\n', '+CPIN: READY')
            # while not gprs_set:
            #     payload = bytearray('AT+CGATT?\r\n', 'ascii')
            #     uart_sim.write(payload)
            #     gprs_set = wait_for_tcp('+CGATT: 1')
            #
            # #commands('AT+CIPSHUT\r\n',None)
            # payload = bytearray('AT+CIPSHUT\r\n', 'ascii')
            # uart_sim.write(payload)
            # if not wait_for_tcp('SHUT OK'):break
            # #commands('AT+CSTT="CMNET"\r\n', 'OK')
            # payload = bytearray('AT+CSTT="CMNET"\r\n', 'ascii')
            # uart_sim.write(payload)
            # if not wait_for_tcp('OK'):break
            # #commands('AT+CIICR\n', 'OK')
            # payload = bytearray('AT+CIICR\n', 'ascii')
            # uart_sim.write(payload)
            # if not wait_for_tcp('OK'): break
            #
            # #commands('AT+CIPSTART="TCP","77.248.232.160","5050"\r\n','connect ok')
            # payload = bytearray('AT+CIPSTART="TCP","77.248.232.160","5050"\r\n', 'ascii')
            # uart_sim.write(payload)
            # wait_for_tcp('CONNECT OK')
            #
            # msg = 'HEAD#END#TAIL'
            # #commands('AT+CIPSEND={:d}\r\n'.format(len(msg)),'>')
            # payload = bytearray('AT+CIPSEND={:d}\r\n'.format(len(msg)), 'ascii')
            # uart_sim.write(payload)
            # wait_for_tcp('>')
            #
            # payload = bytearray(msg, 'ascii')
            # uart_sim.write(payload)
            # #commands(msg,'ok')
            # payload = bytearray('0x1A','ascii')
            # uart_sim.write(payload)
            # # commands('0x1A','ok')
            # wait_for_tcp('')
            # utime.sleep_ms(200)


            pass

    return


# Running on a generic board
#sim_reset()
#utime.sleep_ms(4000)
asyncio.run(main())
